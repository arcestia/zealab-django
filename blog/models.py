from django.db import models
from ckeditor.fields import RichTextField

class Author(models.Model):
	name = models.CharField(max_length=100)
	email = models.EmailField()
	twitter = models.CharField(max_length=100)
	facebook = models.URLField(max_length=100)

	def __str__(self):
	 	return self.name

class Post(models.Model):
	 title = models.CharField(max_length=200)
	 author = models.ForeignKey('author')
	 DJANGO = 'Django'
	 WORDPRESS = 'WordPress'
	 CATATAN = 'Catatan'
	 CATEGORY_CHOICES = (
	 	(DJANGO, 'Django'),
        (WORDPRESS, 'WordPress'),
        (CATATAN, 'Catatan'),
	 )
	 category = models.CharField(max_length=100, choices=CATEGORY_CHOICES)
	 featured_image = models.ImageField(upload_to='images/%Y/%m/%d')
	 description = models.CharField(max_length=255)
	 content = RichTextField(config_name='default')
	 pub_date = models.DateTimeField('date published')

	 def __str__(self):
	 	return self.title