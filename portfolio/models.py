from django.db import models
from ckeditor.fields import RichTextField

class Project(models.Model):
	screenshot = models.ImageField(upload_to='images/%Y/%m/%d')
	title = models.CharField(max_length=200)
	content = RichTextField(config_name='default')
	technology = models.CharField(max_length=200)
	duration = models.CharField(max_length=200)
	budget = models.CharField(max_length=200)
	url = models.URLField()
	pub_date = models.DateTimeField('date published')

	def __str__(self):
		return self.title
