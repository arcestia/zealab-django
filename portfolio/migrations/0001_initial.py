# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('screenshot', models.ImageField(upload_to=b'images/%Y/%m/%d')),
                ('title', models.CharField(max_length=200)),
                ('content', ckeditor.fields.RichTextField()),
                ('technology', models.CharField(max_length=200)),
                ('duration', models.CharField(max_length=200)),
                ('budget', models.CharField(max_length=200)),
                ('url', models.URLField()),
            ],
        ),
    ]
