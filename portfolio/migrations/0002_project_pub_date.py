# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 10, 12, 56, 23, 254276, tzinfo=utc), verbose_name=b'date published'),
            preserve_default=False,
        ),
    ]
