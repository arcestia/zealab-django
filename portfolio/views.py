from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Project

def project_list(request):
	projects = Project.objects.order_by('-pub_date')[:5]
	context = {'projects': projects}
	return render(request, 'portfolio/project_list.html', context)

def project_detail(request, pk):
    project = get_object_or_404(Project, pk=pk)
    return render(request, 'portfolio/project_detail.html', {'project': project})